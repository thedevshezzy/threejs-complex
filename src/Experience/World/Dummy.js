import * as THREE from 'three';
import Experience from '../Experience';
// import { gsap } from 'gsap';

export default class Dummy {
  constructor() {
    this.experience = new Experience();
    this.config = this.experience.config;
    this.camera = this.experience.camera;
    this.debug = this.experience.debug;
    this.time = this.experience.time;
    this.scene = this.experience.scene;
    this.controls = this.experience.controls;
    this.resources = this.experience.resources;
    this.stats = this.experience.stats;

    // Debug
    this.debugFolder = this.debug.addFolder({
      title: 'dummy',
      expanded: false,
    });

    this.setMaterial();
    this.setGeometry();
    this.setMesh();
  }

  setMaterial() {
    this.resources.items.checkboxTexture.magFilter = THREE.NearestFilter;
    this.material = new THREE.MeshBasicMaterial({
      map: this.resources.items.checkboxTexture,
    });
  }

  setGeometry() {
    this.geometry = new THREE.BoxBufferGeometry(1, 1, 1);
  }

  setMesh() {
    // console.log(this.resources.items.blockTexture);
    // this.resources.items.checkboxTexture.minFilter = THREE.NearestFilter;

    this.cube = new THREE.Mesh(this.geometry, this.material);
    this.scene.add(this.cube);
  }

  update() {
    this.cube.position.y = Math.cos(this.time.elapsed * 0.001) * 1;
  }
}
