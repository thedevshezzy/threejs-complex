import * as THREE from 'three';
import Experience from '../Experience';
// import { gsap } from 'gsap';

export default class Lights {
  constructor() {
    this.experience = new Experience();
    this.config = this.experience.config;
    this.camera = this.experience.camera;
    this.debug = this.experience.debug;
    this.time = this.experience.time;
    this.scene = this.experience.scene;
    this.controls = this.experience.controls;
    this.resources = this.experience.resources;
    this.stats = this.experience.stats;

    this.setAmbientLight();
    this.setDirectionalLight();
    // this.setDirectionalLightHelper();
    // this.setDirectionalLightShadowHelper();
  }

  setAmbientLight() {
    this.ambientLight = new THREE.AmbientLight(0x999999, 0.3);
    this.scene.add(this.ambientLight);
  }

  setDirectionalLight() {
    this.directionalLight = new THREE.DirectionalLight(0x999999, 0.5);
    this.directionalLight.position.set(-3, 4, 3);
    this.directionalLight.castShadow = true;
    this.directionalLight.shadow.mapSize.width = 4096;
    this.directionalLight.shadow.mapSize.height = 4096;
    this.directionalLight.shadow.camera.near = 3;
    this.directionalLight.shadow.camera.far = 8;
    this.directionalLight;
    this.scene.add(this.directionalLight);
  }

  setDirectionalLightHelper() {
    this.directionalLightHelper = new THREE.DirectionalLightHelper(
      this.directionalLight
    );
    this.scene.add(this.directionalLightHelper);
  }

  setDirectionalLightShadowHelper() {
    this.directionalLightShadowHelper = new THREE.CameraHelper(
      this.directionalLight.shadow.camera
    );
    this.scene.add(this.directionalLightShadowHelper);
  }

  update() {
    this.cube.position.y = Math.cos(this.time.elapsed * 0.001) * 1;
  }
}
