export default [
  {
    name: 'base',
    data: {},
    items: [
      { name: 'lennaTexture', source: '/assets/lenna.png', type: 'texture' },
      { name: 'blockTexture', source: '/assets/block.png', type: 'texture' },
      {
        name: 'checkboxTexture',
        source: '/assets/checkboard.png',
        type: 'texture',
      },
    ],
  },
];
