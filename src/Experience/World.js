import * as THREE from 'three';
import Experience from './Experience.js';
import Dummy from './World/Dummy';
import Lights from './World/Lights';

export default class World {
  constructor(_options) {
    this.experience = new Experience();
    this.config = this.experience.config;
    this.scene = this.experience.scene;
    this.resources = this.experience.resources;

    this.resources.on('groupEnd', (_group) => {
      if (_group.name === 'base') {
        this.setDummy();
        this.setLights();
      }
    });
  }

  setDummy() {
    this.dummy = new Dummy();
  }

  setLights() {
    this.lights = new Lights();
  }

  resize() {}

  update() {
    if (this.dummy) {
      this.dummy.update();
    }
  }

  destroy() {}
}
